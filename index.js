let http = require("http");
const PORT = 3000;

http
  .createServer((req, res) => {
    if (req.url === "/login") {
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write("Welcome to the Login Page");
      res.end();
    } else {
      res.writeHead(404, { "Content-Type": "text/html" });
      res.write("I'm sorry, the page you are looking for cannot be found");
      res.end();
    }
  })
  .listen(PORT);
console.log("Server is successfully running");
